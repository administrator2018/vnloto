package demo.vnloto.api;

import java.util.ArrayList;
import java.util.List;

public class Services {
	public static String WSURL = "http://localhost:8081/UI.cfm";
	
	public static void main(String[] args) {
		createNewUser("30610", "d01", "123qwe", "123456", "nick name d01", "d01@gmail.com", "123456789", "127.0.0.1");
	}



	public static void createNewUser(String sign, String loginName, String pwd, String paymentPwd, String nickName,
			String email, String phone, String ip) {
		List<Object> olist = new ArrayList<Object>();
		olist.add("UAgentAdvUserRegNew");
		olist.add(sign);
		olist.add(loginName);
		olist.add(pwd);
		olist.add(paymentPwd);
		olist.add(nickName);
		olist.add(email);
		olist.add(phone);
		olist.add(ip);
		try {
			List<Object> rlist = PostDBData.DBPOST(WSURL, olist, 2);
			String res = rlist.get(0).toString();
			String msg = rlist.get(1).toString();
			System.out.println("res = " + res + ", msg =" + msg);
		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}
	}

}
